'use strict';

const express = require('express');

// Constants
const PORT = 3000;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/demo', (req, res) => {
  // Add CSS to center the text and make the entire body red
  const htmlResponse = `
    <html>
      <head>
        <title>Tekton Demo</title>
        <style>
          body {
            background-color: green;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
          }
          h1 {
            color: white;
          }
        </style>
      </head>
      <body>
        <h1>TEKTON-DEMO-V2</h1>
      </body>
    </html>
  `;

  res.send(htmlResponse);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);


